package edu.msu.effendi1.project1;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class ChessPiece {

    /**
     * Top ranks for black and white pieces
     */
    final static float BLACK_TOP_RANK = 0.055f;
    final static float WHITE_TOP_RANK = 0.93f;

    /**
     * IDs for pawns
     */
    final static int whitePawn = R.drawable.chess_plt45;
    final static int blackPawn = R.drawable.chess_pdt45;

    final static CharSequence[] options = {"Queen", "Rook", "Knight", "Bishop"};
    final static List<Integer> whiteOptionsIDs = Arrays.asList(
            R.drawable.chess_qlt45,
            R.drawable.chess_rlt45,
            R.drawable.chess_nlt45,
            R.drawable.chess_blt45
    );
    final static List<Integer> blackOptionsIDs = Arrays.asList(
            R.drawable.chess_qdt45,
            R.drawable.chess_rdt45,
            R.drawable.chess_ndt45,
            R.drawable.chess_bdt45
    );

    /**
     * Chess game manager
     */
    Chess chess;

    /**
     * The image for the actual piece.
     */
    private Bitmap piece;

    /**
     * Tells whether a piece is taken or not
     */
    private boolean taken = false;

    /**
     * The chess piece ID
     */
    private int id;

    /**
     * x location.
     * We use relative x locations in the range 0-1 for the center
     * of the puzzle piece.
     */
    private float x = 0;

    /**
     * y location
     */
    private float y = 0;

    /**
     * x location.
     * We use relative x locations in the range 0-1 for the center
     * of the chess piece.
     */
    private float startX;
    /**
     * y location
     */
    private float startY;

    /**
     * The pieces last location before being moved
     */
    private float lastX;
    private  float lastY;

    /**
     * The piece's original id
     */
    private int origID;

    /**
     * the possible snap locations
     */
    private final ArrayList<Float> snapLocations = new ArrayList<Float>();

    /**
     * Player owner
     */
    int owner;

    /**
     * Chess view Context
     */
    Context chessViewContext = null;

    /**
     * Check if piece is promoted
     */
    Boolean promoted = false;

    /**
     * Denotes if piece can be promoted
     */
    Boolean promotable = false;

    /**
     * We consider a piece to be in the right location if within
     * this distance.
     */
    final static float SNAP_DISTANCE = 0.0625f;


    public ChessPiece(Context context, int id, float startX, float startY, int owner) {
        this.owner = owner;
        this.chessViewContext = context;
        this.startX = startX;
        this.startY = startY;
        this.origID = id;
        this.id = id;

        this.x = startX;
        this.y = startY;

        if(id == whitePawn || id == blackPawn) {
            promotable = true;
        }

        float addition = 0.055f;
        piece = BitmapFactory.decodeResource(context.getResources(), id);
        for(int i = 0; i <= 7; i += 1){
            snapLocations.add(addition);
            addition += .125;
        }
    }

    public void draw(Canvas canvas, int marginX, int marginY, int chessSize, float scaleFactor){
        canvas.save();

        // Convert x,y to pixels and add the margins, then draw
        canvas.translate(marginX + x * chessSize, marginY + y * chessSize);

        // Scale it to the right size
        canvas.scale(scaleFactor, scaleFactor);

        // Center of the piece at 0,0
        canvas.translate(-piece.getWidth() / 2f, -piece.getHeight() / 2f);

        // Draw the bitmap
        canvas.drawBitmap(piece, 0, 0, null);
        canvas.restore();

    }

    /**
     * Test to see if we have touched a puzzle piece
     * @param testX X location as a normalized coordinate (0 to 1)
     * @param testY Y location as a normalized coordinate (0 to 1)
     * @param chessSize the size of the puzzle in pixels
     * @param scaleFactor the amount to scale a piece by
     * @return true if we hit the piece
     */
    public boolean hit(float testX, float testY,
                       int chessSize, float scaleFactor) {

        // Make relative to the location and size to the piece size
        int pX = (int)((testX - x) * chessSize / scaleFactor) +
                piece.getWidth() / 2;
        int pY = (int)((testY - y) * chessSize / scaleFactor) +
                piece.getHeight() / 2;

        if(pX < 0 || pX >= piece.getWidth() ||
                pY < 0 || pY >= piece.getHeight()) {
            return false;
        }

        // We are within the rectangle of the piece.
        // Are we touching actual picture?
        return (piece.getPixel(pX, pY) & 0xff000000) != 0;
    }

    /**
     * Move the puzzle piece by dx, dy
     * @param dx x amount to move
     * @param dy y amount to move
     */
    public void move(float dx, float dy) {
        x += dx;
        y += dy;
    }

    /**
     * If we are within SNAP_DISTANCE of the correct
     * answer, snap to the correct answer exactly.
     * @return true if piece snapped
     */
    public boolean maybeSnap() {
        for(int i = 0; i <= 7; i ++){
            for(int z = 0; z <= 7; z++){
                if(Math.abs(x - snapLocations.get(i)) < SNAP_DISTANCE &&
                        Math.abs(y - snapLocations.get(z)) < SNAP_DISTANCE) {

                    x = snapLocations.get(i);
                    y = snapLocations.get(z);
                    return true;
                }
            }
        }


        return false;
    }

    public void reset() {
        x = startX;
        y = startY;
        taken = false;
    }

    public void checkPromotion() {
        switch (owner) {
            case 1:
                if(Math.abs(y - WHITE_TOP_RANK) < 0.1f && !promoted && promotable) {
                    promoted = true;
                    createAndHandlePromotion(owner);
                }
            case 2:
                if(Math.abs(y - BLACK_TOP_RANK) < 0.1f && !promoted && promotable) {
                    promoted = true;
                    createAndHandlePromotion(owner);
                }
            default:
                break;
        }
    }

    public void createAndHandlePromotion(int owner) {
        final int finalOwner = owner;

        new AlertDialog.Builder(chessViewContext)
                .setTitle(R.string.promotion)
                .setItems(options, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int option) {
                        switch (finalOwner) {
                            case 1:
                                setPiece(whiteOptionsIDs.get(option));
                                break;
                            case 2:
                                setPiece(blackOptionsIDs.get(option));

                        }
                        dialogInterface.dismiss();
                    }
                })
                .show();
    }

//    public void onBoard() {
//        if(x >=  && y != 0){
//
//        }
//    }


    public boolean isTaken(){
        return taken;
    }

    public void gotTaken(){
        taken = true;
    }

    public float getX(){
        return x;
    }

    public float getY(){
        return y;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }

    public int getId() {
        return id;
    }

    public void setPiece(int id) {
        piece = BitmapFactory.decodeResource(chessViewContext.getResources(), id);
        chess.getChessView().reDraw();
    }

    public float getStartX() { return startX; }
    public float getStartY() { return startY; }
    public void setStartX(float x) { startX = x; }
    public void setStartY(float y) { startY = y; }

    public float getLastX() { return lastX; }
    public float getLastY() { return lastY; }
    public void setLastX(float x) { lastX = x; }
    public void setLastY(float y) { lastY = y; }

    public void setChess(Chess chess) {
        this.chess = chess;
    }

}
