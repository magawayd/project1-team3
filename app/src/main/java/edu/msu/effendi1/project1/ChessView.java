package edu.msu.effendi1.project1;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


/**
 * Custom view class for our chess game.
 */
public class ChessView extends View {

    /**
     * The actual game of chess
     */
    private Chess chess;

    private final static String PLAYER = "ChessView.player";
    public int currentPlayer;

    /**
     * Paint for filling the area the puzzle is in
     */
    private Paint fillPaint;

    public ChessView(Context context) {
        super(context);
        init(null, 0);
    }

    public ChessView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs, 0);
    }

    public ChessView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(attrs, defStyle);
    }

    private void init(AttributeSet attrs, int defStyle) {
        chess = new Chess(getContext());
        chess.setChessView(this);

        fillPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        fillPaint.setColor(0xffcccccc);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        chess.draw(canvas);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return chess.onTouchEvent(this, event);
    }

    public boolean isPlayerOneTurn() {
        return chess.playerOneTurn;
    }

    public boolean isPlayerTwoTurn() {
        return chess.playerTwoTurn;
    }


    public int switchTurn() {
        if (isPlayerOneTurn()) {
            chess.playerOneTurn = false;
            chess.playerTwoTurn = true;
            currentPlayer = 1;
            return 1;
        } else {
            chess.playerOneTurn = true;
            chess.playerTwoTurn = false;
            currentPlayer = 2;
            return 2;
        }
    }

//    public void setPlayers(String playerOne, String playerTwo){
//        chess.playerOne = new Player(1,playerOne);
//        chess.playerTwo = new Player(2,playerTwo);
//
//    }


    /**
     * Save the puzzle to a bundle
     * @param bundle The bundle we save to
     */
    public void saveInstanceState(Bundle bundle) {
        bundle.putInt("player", currentPlayer);
        chess.saveInstanceState(bundle);
    }

    /**
     * Load the chess from a bundle
     * @param bundle The bundle we save to
     */
    public void loadInstanceState(Bundle bundle) {
        currentPlayer = bundle.getInt("player");
        chess.loadInstanceState(bundle);
    }

    public void reDraw(){
        invalidate();
    }

    public Chess getChess() {
        return chess;
    }
}