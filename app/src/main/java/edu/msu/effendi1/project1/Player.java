package edu.msu.effendi1.project1;

import android.content.Context;
import android.graphics.BitmapFactory;

import java.io.Serializable;

public class Player implements Serializable {


    private final int id;
    /*
    The name of the player is stored here
     */
    private String name;

    /*
    Shows if it's that players turn or not
     */
    boolean turn;

    /*
    Shows if players won or not
     */
    boolean winner = false;


    Player(int id, String name) {
        this.id = id;
        this.name = name;
        this.turn = id == 1;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
