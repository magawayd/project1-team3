package edu.msu.effendi1.project1;

import android.app.AlertDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

public class Chess {

    /**
     * Percentage of the display width or height that is
     * occupied by the chess board
     */
    final static float SCALE_IN_VIEW = 0.9f;
    /**
     * The name of the bundle keys to save the chess
     */
    private final static String LOCATIONS = "Chess.locations";
    private final static String IDS = "Chess.ids";

    /**
    The first player
     */
    Player playerOne;

    /**
    The second player
     */
    Player playerTwo;
    /**
     * How much we scale the chess pieces
     */
    private float scaleFactor;
    /**
     * Chess board bitmap
     */
    private final Bitmap chessBoard;
    /**
     * Left margin in pixels
     */
    private int marginX;

    /**
     * Top margin in pixels
     */
    private int marginY;
    /**
     * The size of the chess board
     */
    private int chessSize;

    /**
     * Collection of chess pieces
     */
    public ArrayList<ChessPiece> pieces = new ArrayList<ChessPiece>();

    /**
     * This variable is set to a piece we are dragging. If
     * we are not dragging, the variable is null.
     */
    private ChessPiece dragging = null;

    /**
     * This variable is set to a piece that we dragged last.
     */
    private ChessPiece draggingLast = null;

    /**
     * Most recent relative X touch when dragging
     */
    private float lastRelX;

    /**
     * Most recent relative Y touch when dragging
     */
    private float lastRelY;

    /**
     * Chess View Context
     */
    Context chessViewContext = null;

    /**
     * Chess View object
     */
    ChessView chessView;

    public boolean playerOneTurn = true;

    public boolean playerTwoTurn = false;



    /**
     * Chess constructor
     * @param context Object to tell us where to find resources
     */
    public Chess(Context context) {

        this.chessViewContext = context;

        chessBoard =
                BitmapFactory.decodeResource(context.getResources(),
                        R.drawable.chessboard);
        pieces.add(new ChessPiece(context,
                R.drawable.chess_rdt45,
                0.055f,
                0.93f, 2));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_rdt45,
                0.93f,
                0.93f,2));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_ndt45,
                0.18f,
                0.93f,2));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_ndt45,
                0.805f,
                0.93f,2));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_bdt45,
                0.305f,
                0.93f,2));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_bdt45,
                0.68f,
                0.93f,2));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_kdt45,
                0.43f,
                0.93f,2));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_qdt45,
                0.555f,
                0.93f,2));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_pdt45,
                0.055f,
                0.805f,2));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_pdt45,
                0.305f,
                0.805f,2));

        pieces.add(new ChessPiece(context,
                R.drawable.chess_pdt45,
                0.43f,
                0.805f,2));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_pdt45,
                0.555f,
                0.805f,2));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_pdt45,
                0.68f,
                0.805f,2));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_pdt45,
                0.805f,
                0.805f,2));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_pdt45,
                0.93f,
                0.805f,2));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_pdt45,
                0.18f,
                0.805f,2));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_klt45,
                0.43f,
                0.055f,1));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_qlt45,
                0.555f,
                0.055f,1));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_blt45,
                0.68f,
                0.055f,1));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_blt45,
                0.305f,
                0.055f,1));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_nlt45,
                0.18f,
                0.055f,1));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_nlt45,
                0.805f,
                0.055f,1));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_rlt45,
                0.93f,
                0.055f,1));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_rlt45,
                0.055f,
                0.055f,1));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_plt45,
                0.93f,
                0.18f,1));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_plt45,
                0.805f,
                0.18f,1));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_plt45,
                0.68f,
                0.18f,1));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_plt45,
                0.555f,
                0.18f,1));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_plt45,
                0.43f,
                0.18f,1));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_plt45,
                0.305f,
                0.18f,1));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_plt45,
                0.18f,
                0.18f,1));
        pieces.add(new ChessPiece(context,
                R.drawable.chess_plt45,
                0.055f,
                0.18f,1));

        for(ChessPiece piece: pieces) {
            piece.setChess(this);
        }

    }

    /**
     * Draws the chess game
     * @param canvas Drawing object
     */
    public void draw(Canvas canvas) {
        int wid = canvas.getWidth();
        int hit = canvas.getHeight();

        // Determine the minimum of the two dimesions
        int minDim = wid < hit ? wid : hit;

        chessSize = (int)(minDim * SCALE_IN_VIEW);

        // Compute the margins so we center the puzzle
        marginX = (wid - chessSize) / 2;
        marginY = (hit - chessSize) / 2;

        scaleFactor = (float)chessSize /
                (float)chessBoard.getWidth();

        canvas.save();
        canvas.translate(marginX, marginY);
        canvas.scale(scaleFactor, scaleFactor);
        canvas.drawBitmap(chessBoard, 0, 0, null);

        canvas.restore();


        for(ChessPiece piece: pieces) {
            if(!piece.isTaken()){
                piece.draw(canvas, marginX, marginY, chessSize, 0.17f);
            }

        }
    }



    /**
     * Handle a touch event from the view.
     * @param view The view that is the source of the touch
     * @param event The motion event describing the touch
     * @return true if the touch is handled.
     */
    public boolean onTouchEvent(ChessView view, MotionEvent event) {
        //
        // Convert an x,y location to a relative location in the
        // puzzle.
        //
        float relX = (event.getX() - marginX) / chessSize;
        float relY = (event.getY() - marginY) / chessSize;
        switch (event.getActionMasked()) {

            case MotionEvent.ACTION_DOWN:
                return onTouched(relX, relY);

            case MotionEvent.ACTION_UP:
            case MotionEvent.ACTION_CANCEL:
                return onReleased(view, relX, relY);

            case MotionEvent.ACTION_MOVE:
                // Piece being moved is on top of all other pieces
                pieces.remove(dragging);
                pieces.add(dragging);
                // If we are dragging, move the piece and force a redraw
                if(dragging != null & dragging.owner == view.currentPlayer) {
                    dragging.move(relX - lastRelX, relY - lastRelY);
                    lastRelX = relX;
                    lastRelY = relY;
                    view.invalidate();
                    return true;
                }
        }
        return false;
    }

    /**
     * Handle a touch message. This is when we get an initial touch
     * @param x x location for the touch, relative to the puzzle - 0 to 1 over the puzzle
     * @param y y location for the touch, relative to the puzzle - 0 to 1 over the puzzle
     * @return true if the touch is handled
     */
    private boolean onTouched(float x, float y) {

        // Check each piece to see if it has been hit
        // We do this in reverse order so we find the pieces in front
        for(int p=pieces.size()-1; p>=0;  p--) {
            if(pieces.get(p).hit(x, y, chessSize, scaleFactor)) {
                // We hit a piece!
                dragging = pieces.get(p);
                // Save piece as last dragged and save piece's pick-up point
                draggingLast = pieces.get(p);
                pieces.get(p).setLastX(x);
                pieces.get(p).setLastY(y);

                lastRelX = x;
                lastRelY = y;


                return true;
            }
        }

        return false;
    }
    /**
     * Handle a release of a touch message.
     * @param x x location for the touch release, relative to chess - 0 to 1 over the chessboard
     * @param y y location for the touch release, relative to chess - 0 to 1 over the chessboard
     * @return true if the touch is handled
     */
    private boolean onReleased(View view, float x, float y) {

        if(dragging != null) {
            if(dragging.maybeSnap()){
                view.invalidate();
                for (ChessPiece piece: pieces){
                    if(piece.getX() == dragging.getX() &&
                            piece.getY() == dragging.getY() &&
                            piece != dragging && piece.owner != dragging.owner){
                        piece.gotTaken();
                    }
                }
            }
            // If piece last dragged and out of bounds, move back to last position
            for(ChessPiece piece : pieces){
                if(piece == draggingLast){
                    float newX = (lastRelX * chessSize) + marginX;
                    float newY = (lastRelY * chessSize) + marginY;
                    if((newX < marginX || newX > (chessSize + marginX))
                            || (newY < marginY || newY > (chessSize + marginY))){
                        piece.setX(piece.getLastX());
                        piece.setY(piece.getLastY());
                        view.invalidate();
                    }
                }
            }
            dragging = null;
            return true;
        }

        return false;
    }

    public void endGame() {
        for(ChessPiece piece: pieces) {
            piece.reset();
        }
    }

    /**
     * Save the pieces to a bundle
     * @param bundle The bundle we save to
     */
    public void saveInstanceState(Bundle bundle) {
        float [] locations = new float[pieces.size() * 2];
        int [] ids = new int[pieces.size()];

        for(int i=0;  i<pieces.size(); i++) {
            ChessPiece piece = pieces.get(i);
            locations[i*2] = piece.getX();
            locations[i*2+1] = piece.getY();
            ids[i] = piece.getId();
        }
        bundle.putFloatArray(LOCATIONS, locations);
        bundle.putIntArray(IDS,  ids);
    }

    /**
     * Read the chess from a bundle
     * @param bundle The bundle we save to
     */
    public void loadInstanceState(Bundle bundle) {
        float [] locations = bundle.getFloatArray(LOCATIONS);
        int [] ids = bundle.getIntArray(IDS);

        for(int i=0; i<ids.length-1; i++) {

            // Find the corresponding piece
            // We don't have to test if the piece is at i already,
            // since the loop below will fall out without it moving anything
            for(int j=i+1;  j<ids.length;  j++) {
                if(ids[i] == pieces.get(j).getId()) {
                    // We found it
                    // Yah...
                    // Swap the pieces
                    ChessPiece t = pieces.get(i);
                    pieces.set(i, pieces.get(j));
                    pieces.set(j, t);
                }
            }
        }

        for(int i=0;  i<pieces.size(); i++) {
            ChessPiece piece = pieces.get(i);
            piece.setX(locations[i*2]);
            piece.setY(locations[i*2+1]);
        }
    }

    public ArrayList<ChessPiece> getPieces() {
        return pieces;
    }

    public ChessView getChessView() {
        return chessView;
    }

    public void setChessView(ChessView chessView) {
        this.chessView = chessView;
    }
}
