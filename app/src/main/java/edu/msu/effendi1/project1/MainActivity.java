package edu.msu.effendi1.project1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void onStartChess(View view) {
        Intent intent = new Intent(this, ChessActivity.class);
        EditText et1 = findViewById(R.id.playerOne);
        String playerOne = et1.getText().toString();
        EditText et2 = findViewById(R.id.playerTwo);
        String playerTwo = et2.getText().toString();
        Bundle bundle = new Bundle();
        bundle.putString("playerOne", playerOne);
        bundle.putString("playerTwo", playerTwo);
        intent.putExtras(bundle);
        startActivity(intent);
    }

}