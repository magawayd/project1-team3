package edu.msu.effendi1.project1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class GameEndActivity extends AppCompatActivity {

    TextView winner;
    TextView loser;

    String playerWon;
    String playerLose;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_end);

        winner = findViewById(R.id.playerWin);
        loser = findViewById(R.id.playerLose);
        playerWon = getIntent().getExtras().getString("winner");
        playerLose = getIntent().getExtras().getString("loser");
        winner.setText(playerWon + " Wins!");
        loser.setText(playerLose + " Lost...");
    }

    public void onReturnToMain(View view) {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}