package edu.msu.effendi1.project1;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class ChessActivity extends AppCompatActivity {

    TextView playerTurn;

    String playerOneName;
    String playerTwoName;


    int current_player;
    int playerResign;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chess);
        Bundle players = getIntent().getExtras();
       // getChessView().setPlayers(players.getString("playerOne"), players.getString("playerTwo"));
        playerTurn = findViewById(R.id.currentPlayer);
        playerOneName = getIntent().getExtras().getString("playerOne");
        playerTwoName = getIntent().getExtras().getString("playerTwo");
        // playerOne = new Player(1, getIntent().getExtras().getString("playerOne"));
        //playerTwo = new Player(2, getIntent().getExtras().getString("playerTwo"));

        if(savedInstanceState != null) {
            current_player = savedInstanceState.getInt("player");
            // We have saved state
            getChessView().loadInstanceState(savedInstanceState);
            playerTurn = findViewById(R.id.currentPlayer);
            if (current_player == 1){
                playerTurn.setText(playerOneName + ", it is your turn - White!");
                playerResign = 1;

            }else{
                playerTurn.setText(playerTwoName + ", it is your turn - Black!");
                playerResign = 2;
            }
        }


    }
    /**
     * Get the puzzle view
     *
     * @return PuzzleView reference
     */
    private ChessView getChessView() {
        return this.findViewById(R.id.chessView);
    }


    public void onDone(View view){
        current_player = getChessView().switchTurn();
        playerTurn = findViewById(R.id.currentPlayer);
        if (current_player == 1){
            playerTurn.setText(playerOneName + ", it is your turn - White!");
            playerResign = 1;

        }else{
            playerTurn.setText(playerTwoName + ", it is your turn - Black!");
            playerResign = 2;
        }

        for(ChessPiece piece: getChessView().getChess().getPieces()) {
            piece.checkPromotion();
        }
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle bundle) {
        super.onSaveInstanceState(bundle);

        getChessView().saveInstanceState(bundle);
    }

    public void onStartEndGame(View view){
        Intent intent = new Intent(this, GameEndActivity.class);
        Bundle bundle = new Bundle();
        if(playerResign == 1){
            bundle.putString("winner", playerTwoName);
            bundle.putString("loser", playerOneName);
        }else{
            bundle.putString("winner", playerOneName);
            bundle.putString("loser", playerTwoName);
        }
        intent.putExtras(bundle);
      startActivity(intent);
   }

}
